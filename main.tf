terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "http" {} 
}

provider "aws" {
  region = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_security_group" "App_SG" {
  name = var.App_SG
  description = "Allow Web Inbound Traffic"
  vpc_id = var.vpc_id
  ingress =  {
    protocol = "tcp"
    from_port = var.tg_port
    to_port = var.tg_port
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "tls_private_key" "Web-Key" {
  algorithm = "RSA"
}

resource "aws_key_pair" "App-Instance-Key" {
  key_name = "Web-Key"
  public_key = tls_private_key.Web-Key.public_key_openssh
}

resource "aws_instance" "web" {
  ami = var.ami
  instance_type = var.instance_type
  tags = {
    name = "WebServer1"
  }
  count = 1
  subnet_id = var.subnet_id
  key_name = "Web-Key"
  aws_security_group = [aws_security_group.App_SG.id]
  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = ec2-user
      private_key_pem = tls_private_key.Web-Key.private_key_pem
      host = aws_instance.Web[0].public_ip
    }
    inline = [
      "sudo yum install httpd php git -y",
      "sudo systemctl restart httpd",
      "sudo systemctl enable httpd"
    ]
   }
 }

resource "aws_volume_attachment" "attach_ebs" {
  depends_on = [aws_ebs_volume.myebs1]
  device_name = "/dev/sdh"
  volume_id = aws_ebs_volume.myebs1.id
  instance_id = aws_instance.Web[0].id
  force_detach = true
}

resource "null_resource" "nullmount" {
  depends_on = [aws_volume_attachment.attach_ebs]
  connection = {
    type = "ssh"
    user = ec2-user
    private_key_pem = tls_private_key.Web-Key.private_key_pem
    host = aws_instance.Web[0].public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo mkfs.ext4 /dev/xvdh",
      "sudo mount /dev/xvdh /var/www/html",
      "sudo rm -rf /var/www/html/*",
      "sudo git clone https://github.com/ellisonleao/clumsy-bird.git"
    ]
  }
 }